#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtNetwork/QUdpSocket>
#include <QSet>
#include <QTimer>
#include "th_ise113_i.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    void initialize();
    void writeConfig();
    void readConfig();

private slots:
    void on_pushButton_clicked();
    void on_checkbox_localPort_clicked(bool checked);
    void on_lineEdit_remoteIP_changed();
    void on_checkbox_ise113_clicked();
    void on_checkbox_ise113_unchecked();
    void on_timeout();

private:
    Ui::MainWindow *ui;
    QUdpSocket socket;
    QTimer timer;
    QSet<QByteArray> command;
    QSet<QByteArray>::iterator iter;

    TH_ISE113_I *ise113;
};

#endif // MAINWINDOW_H
