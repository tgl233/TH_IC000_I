#include "th_ise113_i.h"
#include "ui_th_ise113_i.h"
#include <QDebug>
#include "convert.h"

TH_ISE113_I::TH_ISE113_I(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TH_ISE113_I)
{
    ui->setupUi(this);
    qDebug("TH_ISE113_I: Constrct...");
    initialize();
}

TH_ISE113_I::~TH_ISE113_I()
{
    delete ui;
}

void TH_ISE113_I::initialize()
{
    qDebug("TH_ISE113_I: initialize...");
//    this->setAttribute(Qt::WA_DeleteOnClose);   //设置属性为关闭时销毁对象
    this->setWindowTitle(tr("TH-ISE113-I 轨道电压相角采集模块"));
    //    this->setModal(true);  //模态对话框
    //    qDebug()<<this->isModal();

    //将所的CheckBox的toggle信号接到setStatus槽上
    connect(ui->checkBox_1,&QCheckBox::toggled,this,&TH_ISE113_I::setBitStatus);
    connect(ui->checkBox_2,&QCheckBox::toggled,this,&TH_ISE113_I::setBitStatus);
    connect(ui->checkBox_3,&QCheckBox::toggled,this,&TH_ISE113_I::setBitStatus);
    connect(ui->checkBox_4,&QCheckBox::toggled,this,&TH_ISE113_I::setBitStatus);
    connect(ui->checkBox_5,&QCheckBox::toggled,this,&TH_ISE113_I::setBitStatus);
    connect(ui->checkBox_6,&QCheckBox::toggled,this,&TH_ISE113_I::setBitStatus);
//    connect(ui->spinBox_index,&QSpinBox::valueChanged,this,&TH_ISE113_I::on_spinBox_index_valueChanged);
    connect(ui->spinBox_index,SIGNAL(valueChanged(int)),this,SLOT(on_spinBox_index_valueChanged(int)));

}

void TH_ISE113_I::setBitStatus(bool checked)
{
    //QObject::sender()来区分不同的信号发送者
    qDebug()<<"QObject::sender(): "<<QObject::sender();
    QCheckBox *pcheckbox = qobject_cast<QCheckBox*>(QObject::sender());
    if(checked){
        pcheckbox->setText(tr("占用"));
    }else{
        pcheckbox->setText(tr("空闲"));
    }
}

void TH_ISE113_I::setStatus(qint16 byteStatue)
{
    if(byteStatue & 1<<0) {ui->checkBox_1->setChecked(true);}else{ui->checkBox_1->setChecked(false);}
    if(byteStatue & 1<<1) {ui->checkBox_2->setChecked(true);}else{ui->checkBox_2->setChecked(false);}
    if(byteStatue & 1<<2) {ui->checkBox_3->setChecked(true);}else{ui->checkBox_3->setChecked(false);}
    if(byteStatue & 1<<3) {ui->checkBox_4->setChecked(true);}else{ui->checkBox_4->setChecked(false);}
    if(byteStatue & 1<<4) {ui->checkBox_5->setChecked(true);}else{ui->checkBox_5->setChecked(false);}
    if(byteStatue & 1<<5) {ui->checkBox_6->setChecked(true);}else{ui->checkBox_6->setChecked(false);}
    if(byteStatue & 1<<6) {ui->checkBox_7->setChecked(true);}else{ui->checkBox_7->setChecked(false);}
}

TH_ISE113_I::DataFrame TH_ISE113_I::toDataFrame(QByteArray array)
{
    TH_ISE113_I::DataFrame ret;
    if(array.size() != 34)
    {
        qDebug()<<"轨道电压相角采集模块数据不完整："<<array.data();
        return ret;
    }

    qint16 *pdata;
    pdata = (qint16 *)array.data();

    ret.JBDY    = (float)*pdata * 0.018;       //电压值 = 值*量程/11000    量程：200
    ret.GD1_DY  = (float)*(pdata+1)*0.01; ret.GD2_DY  = (float)*(pdata+2)*0.01; ret.GD3_DY  = (float)*(pdata+3)*0.01;
    ret.GD4_DY  = (float)*(pdata+4)*0.01; ret.GD5_DY  = (float)*(pdata+5)*0.01; ret.GD6_DY  = (float)*(pdata+6)*0.01;
    ret.GD7_DY  = (float)*(pdata+7)*0.01;
    ret.GD1_XJ  = (float)*(pdata+8)*0.01; ret.GD2_XJ  = (float)*(pdata+9)*0.01; ret.GD3_XJ  = (float)*(pdata+10)*0.01;
    ret.GD4_XJ  = (float)*(pdata+11)*0.01; ret.GD5_XJ  = (float)*(pdata+12)*0.01; ret.GD6_XJ  = (float)*(pdata+13)*0.01;
    ret.GD7_XJ  = (float)*(pdata+14)*0.01;
    ret.JBPL    = (float)*(pdata+15)*0.01;
    ret.KGL     = (float)*(pdata+16);

    return ret;
}

void TH_ISE113_I::setDisplay(DataFrame data)
{
    ui->lineEdit_JB->setText(QString::number(data.JBDY,'f',2));
    ui->lineEdit_DY1->setText(QString::number(data.GD1_DY,'f',2));
    ui->lineEdit_DY2->setText(QString::number(data.GD2_DY,'f',2));
    ui->lineEdit_DY3->setText(QString::number(data.GD3_DY,'f',2));
    ui->lineEdit_DY4->setText(QString::number(data.GD4_DY,'f',2));
    ui->lineEdit_DY5->setText(QString::number(data.GD5_DY,'f',2));
    ui->lineEdit_DY6->setText(QString::number(data.GD6_DY,'f',2));
    ui->lineEdit_DY7->setText(QString::number(data.GD7_DY,'f',2));
    ui->lineEdit_XJ1->setText(QString::number(data.GD1_XJ,'f',2));
    ui->lineEdit_XJ2->setText(QString::number(data.GD2_XJ,'f',2));
    ui->lineEdit_XJ3->setText(QString::number(data.GD3_XJ,'f',2));
    ui->lineEdit_XJ4->setText(QString::number(data.GD4_XJ,'f',2));
    ui->lineEdit_XJ5->setText(QString::number(data.GD5_XJ,'f',2));
    ui->lineEdit_XJ6->setText(QString::number(data.GD6_XJ,'f',2));
    ui->lineEdit_XJ7->setText(QString::number(data.GD7_XJ,'f',2));
    ui->lineEdit_PL->setText(QString::number(data.JBPL,'f',2));

    setStatus(data.KGL);
}

void TH_ISE113_I::clearDisplay()
{
    ui->lineEdit_JB->clear();
    ui->lineEdit_DY1->clear();
    ui->lineEdit_DY2->clear(); ui->lineEdit_DY3->clear(); ui->lineEdit_DY4->clear();
    ui->lineEdit_DY5->clear(); ui->lineEdit_DY6->clear(); ui->lineEdit_DY7->clear();
    ui->lineEdit_XJ1->clear();
    ui->lineEdit_XJ2->clear(); ui->lineEdit_XJ3->clear(); ui->lineEdit_XJ4->clear();
    ui->lineEdit_XJ5->clear(); ui->lineEdit_XJ6->clear(); ui->lineEdit_XJ7->clear();
    ui->lineEdit_PL->clear();
    setStatus(0);
}

QByteArray TH_ISE113_I::Data() const
{
    return this->data;
}

void TH_ISE113_I::setData(QByteArray dat)
{
    qDebug()<<"setData";
    if(dat.size() % 34)
    {
        qDebug()<<"数据包长度不正确！";
    }
    else
    {
        qDebug()<<dat.size();
        if(dat.size())
        {
            this->data = dat;
            ui->spinBox_index->setRange(1,dat.size()/34);
            //更新显示
//            ui->spinBox_index->setValue(ui->spinBox_index->value());
            setDisplay(toDataFrame(data.mid((ui->spinBox_index->value()-1)*34,34)));
        }
        else
        {            
            ui->spinBox_index->setRange(0,0);
           this->clearDisplay();
        }

    }
}

void TH_ISE113_I::on_spinBox_index_valueChanged(int arg1)
{
    qDebug()<<"on_spinBox_index_valueChanged";
    if(this->data.size() != 0)
    {
        setDisplay(toDataFrame(data.mid((arg1-1)*34,34)));
    }
    else
    {
        clearDisplay();
    }
}
