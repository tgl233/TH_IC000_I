#-------------------------------------------------
#
# Project created by QtCreator 2016-05-02T22:35:58
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TH_IC000_I
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    th_ise113_i.cpp \
    convert.cpp \
    th_ise130_i.cpp

HEADERS  += mainwindow.h \
    th_ise113_i.h \
    convert.h \
    th_ise130_i.h

FORMS    += mainwindow.ui \
    th_ise113_i.ui
