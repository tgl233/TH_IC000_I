#ifndef CONVERT_H
#define CONVERT_H
#include <QApplication>

class Convert
{
public:
    Convert();

    static QByteArray toByteArray (QString hexString);
    static QString toHexString(QByteArray arry);
};

#endif // CONVERT_H
