#ifndef TH_ISE113_I_H
#define TH_ISE113_I_H

#include <QDialog>

namespace Ui {
class TH_ISE113_I;
}

class TH_ISE113_I : public QDialog
{
    Q_OBJECT
    Q_PROPERTY(QByteArray Data READ Data WRITE setData )

public:
    //轨道电压相角采集模块的数值结构体定义
    struct _dataFrame{
        float JBDY;        //局部电压值
        float GD1_DY;      //第1路轨道电压值
        float GD2_DY; float GD3_DY; float GD4_DY;
        float GD5_DY; float GD6_DY; float GD7_DY;
        float GD1_XJ;      //第1路轨道相角值
        float GD2_XJ; float GD3_XJ; float GD4_XJ;
        float GD5_XJ; float GD6_XJ; float GD7_XJ;
        float JBPL;       //局部频率
        qint16 KGL;       //7路轨道的开关量状态

        //默认构造函数
        _dataFrame()
        {
            JBDY = 0;
            GD1_DY = 0; GD2_DY = 0; GD3_DY = 0;
            GD4_DY = 0; GD5_DY = 0; GD6_DY = 0; GD7_DY = 0;
            GD1_XJ = 0; GD2_XJ = 0; GD3_XJ = 0;
            GD4_XJ = 0; GD5_XJ = 0; GD6_XJ = 0; GD7_XJ = 0;
            JBPL = 0;
            KGL = 0;
        }
    };
    typedef _dataFrame DataFrame;   //定义结构体数据类型DataFrame;

public:
    explicit TH_ISE113_I(QWidget *parent = 0);
    ~TH_ISE113_I();

    QByteArray Data() const;
    void setData(QByteArray dat);

    static DataFrame toDataFrame(QByteArray array);
    
private:
    void initialize();
    void setStatus(qint16 byteStatue);
    void setDisplay(DataFrame data);
    void clearDisplay();
    void setBitStatus(bool checked);
//    void on_spinbox_changed(int index);

private slots:
    void on_spinBox_index_valueChanged(int arg1);

private:
    Ui::TH_ISE113_I *ui;
    QByteArray data;
};

#endif // TH_ISE113_I_H
